package GoT

import GoT.internals.{Move, MoveAttack, MoveUpgrade}

import scala.annotation.tailrec
import scala.io.StdIn.readLine

object GameOfThronesApp extends App {
  var engine = GameOfThrones()

  println("----! Welcome to Game of Thrones your majesties !----")

  run()

  private def run(): Unit = {
    @tailrec
    def getMoveT: Move = {
      print("What is your order: ")
      val c = readLine().toInt
      println(c)
      c match {
        case 1 =>
          val tryWealth = engine.targaryen.visitBraavos getOrElse engine.targaryen.wealth
          MoveUpgrade(tryWealth)
        case 2 =>
          val tryWealth = engine.targaryen.dracarys
          if (tryWealth.isDefined)
            MoveUpgrade(tryWealth.get)
          else getMoveT
        case 3 =>
          MoveAttack(engine.targaryen.attack)
      }
    }

    @tailrec
    def getMoveL: Move = {
      print("What is your order: ")
      readLine().toInt match {
        case 1 =>
          val tryWealth = engine.lannister.visitBraavos getOrElse engine.lannister.wealth
          MoveUpgrade(tryWealth)
        case 2 =>
          val tryWealth = engine.lannister.burnThemAll
          if (tryWealth.isDefined)
            MoveUpgrade(tryWealth.get)
          else getMoveL
        case 3 =>
          MoveAttack(engine.lannister.attack)
      }
    }

    @tailrec
    def gameTurnLoop(): Unit = {
      println(s"\n---- Turn ${engine.turn} ----")
      println(engine.targaryen)
      println(engine.lannister)
      println(
        s"""
           |House of ${engine.targaryen.name}, now is your turn
           |[1] - Borrow money from Iron Bank
           |[2] - Prepare your dragons
           |[3] - Attack!
           |""".stripMargin)

      val tMove = getMoveT

      println(
        s"""
           |House of ${engine.lannister.name}, now is your turn
           |[1] - Borrow money from Iron Bank
           |[2] - Boil dragonfire
           |[3] - Attack!
           |""".stripMargin)

      val lMove = getMoveL

      val nextEngine = engine.nextTurn(tMove)(lMove)

      if (nextEngine.isDefined) {
        engine = nextEngine.get
        gameTurnLoop()
      }
    }

    gameTurnLoop()
  }
}
