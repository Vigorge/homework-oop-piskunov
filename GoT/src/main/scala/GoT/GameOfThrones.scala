package GoT

import GoT.greathouse.GreatHouseWealth
import GoT.greathouse.houses.{Lannister, Targaryen}
import GoT.internals.{Move, MoveAttack, MoveUpgrade}

trait Snap {
  def targaryen: Targaryen
  def lannister: Lannister
  def turn: Int
  def nextTurn: Move => Move => Option[Snap]
}

private class GameOfThronesSnap(val targaryen: Targaryen, val lannister: Lannister, val turn: Int) extends Snap {
  override def nextTurn: Move => Move => Option[Snap] = (targaryenMove: Move) => (lannisterMove: Move) => {
    var targaryenWealth = targaryen.wealth
    var lannisterWealth = lannister.wealth
    targaryenMove match {
      case attackMove: MoveAttack =>
        val attackEffect = attackMove.attack(lannister)
        println(s"House of ${targaryen.name} attacked House of ${lannister.name}")
        if (attackEffect.badLuck) {
          targaryenWealth = GreatHouseWealth(targaryenWealth.treasury, (targaryenWealth.forces * 0.5).toInt)
          println(s"${targaryen.name}: ${targaryen.badLuckMsg}")
        }
        if (attackEffect.critical) {
          lannisterWealth = GreatHouseWealth(lannisterWealth.treasury, lannisterWealth.forces - 10 - (lannisterWealth.forces * 0.2).toInt)
          println(s"${targaryen.name}: ${targaryen.badLuckMsg}")
        }
        lannisterWealth = GreatHouseWealth(lannisterWealth.treasury - attackEffect.captured, lannisterWealth.forces - attackEffect.killed, lannisterWealth.appliedAbility)
        targaryenWealth = GreatHouseWealth(targaryenWealth.treasury + attackEffect.captured, targaryenWealth.forces, targaryenWealth.appliedAbility)
      case upgradeMove: MoveUpgrade =>
        targaryenWealth = upgradeMove.wealth
    }
    lannisterMove match {
      case attackMove: MoveAttack =>
        val attackEffect = attackMove.attack(targaryen)
        println(s"House of ${lannister.name} attacked House of ${targaryen.name}")
        if (attackEffect.badLuck) {
          lannisterWealth = GreatHouseWealth(lannisterWealth.treasury, (lannisterWealth.forces * 0.8).toInt)
          println(s"${lannister.name}: ${lannister.badLuckMsg}")
        }
        if (attackEffect.critical) {
          targaryenWealth = GreatHouseWealth(targaryenWealth.treasury, (targaryenWealth.forces * 0.8).toInt)
          println(s"${lannister.name}: ${lannister.badLuckMsg}")
        }
        targaryenWealth = GreatHouseWealth(targaryenWealth.treasury - attackEffect.captured, targaryenWealth.forces - attackEffect.killed, targaryenWealth.appliedAbility)
        lannisterWealth = GreatHouseWealth(lannisterWealth.treasury + attackEffect.captured, lannisterWealth.forces, lannisterWealth.appliedAbility)
      case upgradeMove: MoveUpgrade =>
        lannisterWealth = upgradeMove.wealth
    }
    if (lannisterWealth.treasury < 0) {
      println(s" House of ${lannister.name} can't pay their debts anymore")
      None
    } else if (lannisterWealth.forces < 0) {
      println(s" House of ${lannister.name} has no army to rule")
      None
    } else if (targaryenWealth.treasury < 0) {
      println(s" House of ${targaryen.name}'s valyrian treasures are gone")
      None
    } else if (targaryenWealth.forces < 0) {
      println(s" House of ${targaryen.name} lost all their dragons")
      None
    } else {
      println(targaryenWealth.forces)
      targaryenWealth = GreatHouseWealth(targaryenWealth.treasury + 5, targaryenWealth.forces + 5, targaryenWealth.appliedAbility)
      lannisterWealth = GreatHouseWealth(lannisterWealth.treasury + 5, lannisterWealth.forces + 5, lannisterWealth.appliedAbility)
      Some(new GameOfThronesSnap(Targaryen(targaryenWealth), Lannister(lannisterWealth), turn + 1))
    }
  }
}

object GameOfThrones {
  def apply(): Snap = {
    new GameOfThronesSnap(Targaryen(), Lannister(), turn = 1)
  }
}
