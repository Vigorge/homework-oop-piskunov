package GoT.greathouse

trait Wealth {
  def treasury: Int
  def forces: Int
  def appliedAbility: Boolean
}

case class GreatHouseWealth(treasury: Int, forces: Int, appliedAbility: Boolean = false) extends Wealth
