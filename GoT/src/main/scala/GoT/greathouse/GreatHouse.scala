package GoT.greathouse

trait GreatHouse {
  def name: String
  def wealth: Wealth

  override def toString: String =
    s"""
       |[ ]--------{House of $name}--------[ ]
       | | Treasury: ${wealth.treasury}  Forces: ${wealth.forces}
       |[ ]--------{House of $name}--------[ ]
       |""".stripMargin
}