package GoT.greathouse.houses

import GoT.abilities.{Attack, AttackEffect, BorrowMoney, CallDragon}
import GoT.greathouse.{GreatHouse, GreatHouseWealth, Wealth}

case class Targaryen(wealth: Wealth = GreatHouseWealth(80, 80)) extends GreatHouse with BorrowMoney with CallDragon with Attack {
  override val name: String = "Targaryen"
  override val criticalMsg = "Dragon destroyed enemy's supplies!"
  override val badLuckMsg = "Dragon fell from the sky!"

  override def attack(enemy: GreatHouse): AttackEffect = {
    val r = scala.util.Random
    val captured = (enemy.wealth.treasury * r.nextDouble * 0.5).toInt
    val killed = (wealth.forces * 0.3).toInt
    val critical = wealth.appliedAbility && r.nextDouble <= 0.4
    val badLuck = wealth.appliedAbility && r.nextDouble <= 0.1
    AttackEffect(captured, killed, critical, badLuck)
  }
}
