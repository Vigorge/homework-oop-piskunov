package GoT.greathouse.houses

import GoT.abilities.{Attack, AttackEffect, BorrowMoney, CallDragon, MakeWildFire}
import GoT.greathouse.{GreatHouse, GreatHouseWealth, Wealth}

case class Lannister(wealth: Wealth = GreatHouseWealth(140, 100)) extends GreatHouse with BorrowMoney with MakeWildFire with Attack {
  override val name: String = "Lannister"
  override val criticalMsg = "Wildfire was extremely effective!"
  override val badLuckMsg = "Wildfire reserves blew up! Our troops are burning!"

  override def attack(enemy: GreatHouse): AttackEffect = {
    val r = scala.util.Random
    val captured = (enemy.wealth.treasury * r.nextDouble * 0.6).toInt
    val killed = (wealth.forces * 0.3).toInt
    val critical = wealth.appliedAbility && r.nextDouble <= 0.4
    val badLuck = wealth.appliedAbility && r.nextDouble <= 0.1
    AttackEffect(captured, killed, critical, badLuck)
  }
}
