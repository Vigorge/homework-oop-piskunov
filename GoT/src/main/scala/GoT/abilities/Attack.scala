package GoT.abilities

import GoT.greathouse.GreatHouse

trait Attack {
  def attack(enemy: GreatHouse): AttackEffect
  def badLuckMsg: String
  def criticalMsg: String
}

case class AttackEffect(captured: Int, killed: Int, critical: Boolean, badLuck: Boolean)
