package GoT.abilities

import GoT.greathouse.{GreatHouse, GreatHouseWealth, Wealth}

trait MakeWildFire {
  this: GreatHouse =>
  def burnThemAll: Option[Wealth] =
    if (wealth.treasury >= 10) {
      println("""BURN THEM ALL!""")
      Some(GreatHouseWealth(wealth.treasury - 10, (wealth.forces + wealth.forces*0.3).toInt, appliedAbility = true))
    } else {
      println("""You cannot afford more wildfire""")
      None
    }
}