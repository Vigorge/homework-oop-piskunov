package GoT.abilities

import GoT.greathouse.{GreatHouse, GreatHouseWealth, Wealth}

trait CallDragon {
  this: GreatHouse =>
  def dracarys: Option[Wealth] =
    if (!wealth.appliedAbility) {
      println("""You prepared your dragons to fight enemies of the throne""")
      Some(GreatHouseWealth(wealth.treasury, wealth.forces*2, appliedAbility = true))
    } else {
      println("""Your dragons are already armed, but they are glad to see you""")
      None
    }
}