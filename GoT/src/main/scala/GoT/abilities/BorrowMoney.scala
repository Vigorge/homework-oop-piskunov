package GoT.abilities

import GoT.greathouse.{GreatHouse, GreatHouseWealth, Wealth}

trait BorrowMoney {
  this: GreatHouse =>
  def visitBraavos: Option[Wealth] = {
    val r = scala.util.Random
    val rich_enough = ((wealth.treasury / 100.0) min 1.0) > r.nextDouble()
    if (rich_enough) {
      println("""Iron Bank borrowed you 30 of gold""")
      Some(GreatHouseWealth(wealth.treasury + 30, wealth.forces))
    } else {
      println("""Iron Bank refused to give you a loan""")
      None
    }
  }
}