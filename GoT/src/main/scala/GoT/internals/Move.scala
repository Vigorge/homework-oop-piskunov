package GoT.internals

import GoT.abilities.AttackEffect
import GoT.greathouse.{GreatHouse, Wealth}

sealed trait Move

case class MoveAttack(attack: GreatHouse => AttackEffect) extends Move

case class MoveUpgrade(wealth: Wealth) extends Move
